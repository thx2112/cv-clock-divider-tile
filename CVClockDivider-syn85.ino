/***
*       _______    __   ________           __      ____  _       _     __
*      / ____| |  / /  / ____/ ____  _____/ /__   / __ \(__   __(_____/ ___  _____
*     / /    | | / /  / /   / / __ \/ ___/ //_/  / / / / | | / / / __  / _ \/ ___/
*    / /___  | |/ /  / /___/ / /_/ / /__/ ,<    / /_/ / /| |/ / / /_/ /  __/ /
*    \____/  |___/   \____/_/\____/\___/_/|_|  /_____/_/ |___/_/\__,_/\___/_/
*
*
*
*								***THX2112***
*
*							http://syinsi.com
*
*				10/26/2016  Modded for syn85 module.
*				01/15/2017	Reduced clock jitter, added "mathematical" timings, improved boot config.
*
*
*
*/

#include <EEPROM.h>

#define F_CPU 16000000UL
#include <avr/io.h>
#include <EEPROM.h>

#define clockOutPin 3			//	PB3
#define clockInPin	1			//	PB1
#define resetInPin	0			//	PB0
#define dividePin	A1
#define LEDPin		4			//	PB4

int clockPulse;
int tempoValue;
int potMap;
int clockDivMult;
int bounceTimer = 0;
int lastBounceTime = 0;
int i = 0;
bool disableLED;
unsigned long timeoutTimer = 0;		//	microseconds
unsigned long previousPulse = 0;	//	microseconds
unsigned long currentPulse = 0;		//	microseconds
unsigned long periodStartTime = 0;	//	microseconds
unsigned long periodEndTime = 0;	//	microseconds
bool trigState = LOW;
bool lastTrigState = LOW;
bool resetState = LOW;
bool lastResetState = LOW;
bool startState = LOW;
bool lastStartState = LOW;
unsigned long duration;				//	microseconds
bool isHit = false;
unsigned long beginTime;
unsigned long now;
int LEDCounter = 0;
bool LEDHit = false;
int timing;
int clockTotal = 96;

///////////////////////////////////////////////////////////////////////////////
//
//	Setup pins.
//

void setup()
{
	pinMode(LEDPin, OUTPUT);
	pinMode(clockOutPin, OUTPUT);
	pinMode(resetInPin, INPUT);
	pinMode(clockInPin, INPUT);

	checkSetup();

	timing = EEPROM.read(0);

	if (timing == 2)			 //	Not 24PPQN
	{
		clockTotal = 128;
	}

	// Flash LED to show we're alive.
	digitalWrite(LEDPin, HIGH);
	delay(100);
	digitalWrite(LEDPin, LOW);

	getTempo(); // Prime the pump.
}

///////////////////////////////////////////////////////////////////////////////
//
//	Main.
//

void loop()
{
	checkTrigger();
}

///////////////////////////////////////////////////////////////////////////////
//
//	Time stuff. See if trigger is being hit...
//

void checkTrigger()
{
	//
	//	Check CLOCK and RESET pins.
	//

	trigState = checkClockPin();
	resetState = checkResetPin();
	//startState = digitalRead(startPin);					//	old "start" code left in case we reuse it later.
	startState = HIGH;			//  Start isn't used, so always HIGH.

	if (resetState == HIGH && lastResetState == LOW)		//	Reset clock if reset pin pulled high
	{
		clockPulse = 0;
		LEDCounter = 0;
		lastResetState = HIGH;
		getTempo();
		isHit = true;	//	All hits start on downbeat. Prevents skipping the first hit after a start or reset
	}

	//	if (startState == HIGH && lastStartState == LOW)	//	Reset clock if this is first new START
	//	{
	//		clockPulse = 0;
	//		lastStartState = HIGH;
	//		getTempo();
	//		isHit = true;	//	All hits start on downbeat. Prevents skipping the first hit after a start or reset
	//	}

	if ((trigState == HIGH) && (lastTrigState == LOW))		//	If a new clock is detected...
	{
		clockPulse++;

		if (clockPulse > clockTotal)	//	Clocks start at one. Sync 24 = 24ppqn = 96 pulses per bar. Reset.
		{
			clockPulse = 1;
		}

		//
		//	Send the hit if it's time and START is held high. This is done here to minimize latency.
		//

		if (isHit)
		{
			if (startState)
			{
				hitIt();
			}
		}

		lastTrigState = HIGH;
	}

	//
	//	Reset state toggles.
	//

	if ((trigState == LOW) && (lastTrigState == HIGH))	//	This is a good place for time consuming code like maths.

	{
		getTempo();					//	This is slow so do this here while waiting for next clock.
		checkNextHit();				//	Check if next clock will be a hit here to reduce latency.
		lastTrigState = LOW;
	}

	if ((resetState == LOW) && (lastResetState == HIGH))
	{
		lastResetState = LOW;
	}

	//	if ((startState == LOW) && (lastStartState == HIGH))
	//	{
	//		lastStartState = LOW;
	//	}
}

///////////////////////////////////////////////////////////////////////////////
//
//	Send the new clock.
//

void hitIt()
{
	startPulse();				//	Rising edge of new output clock.
	isHit = false;				//	Reset.
	beginTime = micros();
	now = 0;
	duration = 5000;			//	Pulse duration in microseconds. 5000us = 5ms
	bool doOnce = true;
	clockPulse = 0;				//	Each pulse restarts clock.
	if (LEDHit) {
		startLED();
		LEDHit = false;
	}

	//
	//	Loop until the end of DURATION.
	//

	while (now < duration)
	{
		//	Set division here for reduced latency
		if (doOnce)
		{
			getTempo();
			doOnce = false;
		}

		now = (micros() - beginTime);	//	Register time for next run through.
	}

	//
	//	Clean up at end of pulse.
	//

	endPulse();	//	Falling edge of new clock.
	endLED();
}

//
//	Set division
//

int getTempo()
{
	tempoValue = analogRead(dividePin);		//	Slow.

	if (timing == 1) {
		potMap = map(tempoValue, 0, 1023, 13, 0);	// Reverse response of pot and map to X values

		//	A Smörgåsbord of Odd and Even Timings

		if (potMap == 13) { clockDivMult = 96; }	//	1	Every bar.
		if (potMap == 12) { clockDivMult = 64; }	//	1.5 Causes uneven beats due to retriggering at end of 96-tick bar. (FIXED)
		if (potMap == 11) { clockDivMult = 48; }	//	2	half
		if (potMap == 10) { clockDivMult = 32; }	//	3	third. Causes uneven beats due to retriggering at end of 96-tick bar. (FIXED)
		if (potMap == 9) { clockDivMult = 24; }		//	4	quarter
		if (potMap == 8) { clockDivMult = 16; }		//	6
		if (potMap == 7) { clockDivMult = 12; }		//	8th note -- default setting
		if (potMap == 6) { clockDivMult = 8; }
		if (potMap == 5) { clockDivMult = 6; }		//	16th notes
		if (potMap == 4) { clockDivMult = 5; }		//
		if (potMap == 3) { clockDivMult = 4; }		//	24	fast
		if (potMap == 2) { clockDivMult = 3; }		//	32	really really fast
		if (potMap == 1) { clockDivMult = 2; }		//	48	something is going to break
		if (potMap == 0) { clockDivMult = 1; }		//	96	every 24ppqm clock tick (may fracture spacetime.)
	}

	if (timing == 0) {
		potMap = map(tempoValue, 0, 1023, 6, 0);	// Reverse response of pot and map to X values

		//	Just (boring) Even Timings

		if (potMap == 6) { clockDivMult = 96; }
		if (potMap == 5) { clockDivMult = 48; }
		if (potMap == 4) { clockDivMult = 24; }
		if (potMap == 3) { clockDivMult = 12; }
		if (potMap == 2) { clockDivMult = 6; }
		if (potMap == 1) { clockDivMult = 3; }
		if (potMap == 0) { clockDivMult = 1; }
	}

	if (timing == 2) {
		potMap = map(tempoValue, 0, 1023, 7, 0);	// Reverse response of pot and map to X values

		//	Mathematical Timings for Robomusicians. Uses 128 total clocks instead of 96.

		if (potMap == 7) { clockDivMult = 128; }
		if (potMap == 6) { clockDivMult = 64; }
		if (potMap == 5) { clockDivMult = 32; }
		if (potMap == 4) { clockDivMult = 16; }
		if (potMap == 3) { clockDivMult = 8; }
		if (potMap == 2) { clockDivMult = 4; }
		if (potMap == 1) { clockDivMult = 2; }
		if (potMap == 0) { clockDivMult = 1; }
	}

	//checkHit();

	return clockDivMult;
}

void checkHit()
{
	if ((clockPulse % clockDivMult == 0))	//	Slow.
	{
		isHit = true;
	}
}

void checkNextHit()
{
	if (((clockPulse + 1) % clockDivMult == 0))	//	Slow.
	{
		isHit = true;
	}

	if ((clockPulse + 1 == 48) || (clockPulse + 1 == 24) || (clockPulse + 1 == 1) || (clockPulse + 1 == 96)) {
		LEDHit = true;
	}
}

/***********************************************************************************************************

				FAST IO

*/

void startPulse() {
	PORTB |= (1 << clockOutPin);							// clockOutputPin high
}

void endPulse() {
	PORTB &= ~(1 << clockOutPin);							// clockOutputPin low
}

void startLED() {
	if (!disableLED) {
		PORTB |= (1 << LEDPin);								// LEDPin high
	}
}

void endLED() {
	if (!disableLED) {
		PORTB &= ~(1 << LEDPin);							// LEDPin low
	}
}

boolean checkResetPin()
{
	return (bitRead(PINB, resetInPin));
}

boolean checkClockPin()
{
	return (bitRead(PINB, clockInPin));
}

/**********************************************************************************************************

				Boot-time Configuration:

-Turn off module
-Patch from input to output (first jack to third jack).
-Turn pot to desired programming position (left, center, right).
	-Full left for odd and even timings, center for mathematical timings, full right for only even timings.
-Turn power on.
-Module LED will flash for 1 second to show it's been programmed.
-Remove patch cable.
-Module should function as desired, but may need to be power-cycled again (without patch cable).

**********************************************************************************************************/

void checkSetup()
{
	bool fail = false;										//	Optimism is everything

	//	Start pulse sequence to check if setup patch cables enabled

	for (int i = 1; i <= 10; i++)
	{
		digitalWrite(clockOutPin, HIGH);
		delay(random(10) + 1);								//	Not random, but generates a sequence
		if (digitalRead(clockInPin) != HIGH) { fail = true; }
		digitalWrite(clockOutPin, LOW);
		delay(random(10) + 1);
		if (digitalRead(clockInPin) != LOW) { fail = true; }
	}

	//	If output is connected to input, read the pot and store the value

	if (!fail)
	{
		//	Set timings
		int pot = analogRead(dividePin);
		if (pot <= 300) {
			EEPROM.write(0, 1);								//	Location 0, Include odd timings
		}
		else if (pot >= 800) {
			EEPROM.write(0, 0);								//	Location 0, No odd timings
		}
		else {
			EEPROM.write(0, 2);								//	Location 0, Mathematical timings
		}

		digitalWrite(LEDPin, HIGH);
		delay(1000);
		digitalWrite(LEDPin, LOW);
		delay(500);
	}

	//	If EEPROM is empty (ie. new firmware uploaded).
	if (EEPROM.read(0) == 255) {
		EEPROM.write(0, 0);
		digitalWrite(LEDPin, HIGH);
		delay(100);
		digitalWrite(LEDPin, LOW);
		delay(100);
		digitalWrite(LEDPin, HIGH);
		delay(100);
		digitalWrite(LEDPin, LOW);
		delay(100);
	}
}